<?php
    // Заголовки
    header('Content-Type: text/html; charset=UTF-8');

    // Параметры для подключения
    $db_host = "localhost";
    $db_user = "u16407";// логин базы данных
    $db_password = "6824014"; // пароль базы
    $db_base = "u16407"; // имя базы
    $db_table = "app"; // имя таблицы базы

    // Переменные
    $name = $_POST['name'];
    $email = $_POST['email'];
    $age = $_POST['age'];
    $sex = $_POST['sex'];
    $limb = $_POST['limbs'];
    $bio = $_POST['bio'];
    $check = $_POST['check'];
    $super = array();
    foreach ($_POST['super'] as $key => $value) {
        $super[$key] = $value;
    }
    $powers_string = implode(', ', $super);


    // Подключение к базе данных
    $mysqli = new mysqli($db_host, $db_user, $db_password, $db_base);

    // Если есть ошибка соединения, выводим её и убиваем подключение
    if ($mysqli->connect_error) {
        die('Ошибка подключения к базе данных : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }

    // Если пользователь не ввел имя, сообщаем ему об этом и убиваем подключение
    if ($name == "") {
        die("Ошибка! Введите своё имя.");
    }

    // Если пользователь не ввел почту, сообщаем ему об этом и убиваем подключение
    if ($email == "") {
        die("Ошибка! Введите свой email.");
    }

    // Если пользователь не ввел биографию, сообщаем ему об этом и убиваем подключение
    if ($bio == "") {
        die("Ошибка! Напишите что-нибудь о себе.");
    }

    // Если пользователь не поставил галочку, сообщаем ему об этом и убиваем подключение
    if ($check != "on") {
        die("Вы не можете отправить форму, если не согласны с контрактом.");
    }

    // Создаем запрос в базу данных и записываем его в переменную
    $result = $mysqli->query("INSERT INTO ".$db_table." (name, email, age, sex, limb, super, bio) VALUES ('$name','$email',$age,'$sex',$limb,'$powers_string','$bio')");

    // Проверка
    if ($result == true) {
        echo "<script>alert('Успешно отправлено!')</script>";
        header('Refresh: 1; url=/web-backend3/public/');
    } else {
        echo "Ошибка! Информация не занесена в базу данных";
    }
?>