CREATE TABLE app (
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    email varchar(256) NOT NULL,
    age int(4) UNSIGNED NOT NULL,
    sex varchar(6) NOT NULL,
    limbs int(2) UNSIGNED NOT NULL,
    powers varchar(512) NULL,
    bio varchar(512) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO app (name, email, age, sex, limbs, powers, bio) VALUES ("Nick", "nick2000@mail.ru", 2000, "male", 4, "super", "Hello!");

INSERT INTO app (name, email, age, sex, limbs, powers, bio) VALUES ("Kristina", "kris@mail.com", 2000, "female", 8, "super", "Hi!");

INSERT INTO app VALUES (0, "user", "email@email.com", 2000, "male", 4, "", "biography");
